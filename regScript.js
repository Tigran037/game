let form = document.getElementById("objectForm");

form.addEventListener("submit", e => {
    e.preventDefault();
    let obj = {};
    Object.keys(form.elements).forEach(key => {
        let element = form.elements[key];
        if (element.type !== "submit") {
            obj[element.name] = element.value;
        }
    });
    console.log(obj);
});
